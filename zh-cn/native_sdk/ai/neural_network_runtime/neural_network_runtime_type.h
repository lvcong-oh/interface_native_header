/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup NeuralNeworkRuntime
 * @{
 *
 * @brief 提供Neural Network Runtime加速模型推理的相关接口。
 *
 * @since 9
 * @version 2.0
 */

/**
 * @file neural_network_runtime_type.h
 *
 * @brief Neural Network Runtime定义的结构体和枚举值。
 *
 * 引用文件"neural_network_runtime/neural_network_runtime_type.h"
 * @library libneural_network_runtime.so
 * @Syscap SystemCapability.Ai.NeuralNetworkRuntime
 * @since 9
 * @version 2.0
 */

#ifndef NEURAL_NETWORK_RUNTIME_TYPE_H
#define NEURAL_NETWORK_RUNTIME_TYPE_H

#include <cstddef>
#include <cstdint>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 模型句柄。
 *
 * @since 9
 * @version 1.0
 */
typedef struct OH_NNModel OH_NNModel;

/**
 * @brief 编译器句柄。
 *
 * @since 9
 * @version 1.0
 */
typedef struct OH_NNCompilation OH_NNCompilation;

/**
 * @brief 执行器句柄。
 *
 * @since 9
 * @version 1.0
 */
typedef struct OH_NNExecutor OH_NNExecutor;

/**
 * @brief 量化参数的句柄。
 *
 * @since 11
 * @version 1.0
 */
typedef struct NN_QuantParam NN_QuantParam;

/**
 * @brief Tensor描述的句柄。
 *
 * @since 11
 * @version 1.0
 */
typedef struct NN_TensorDesc NN_TensorDesc;

/**
 * @brief Tensor句柄。
 *
 * @since 11
 * @version 1.0
 */
typedef struct NN_Tensor NN_Tensor;

/**
 * @brief 硬件的性能模式。
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** 无性能模式偏好 */
    OH_NN_PERFORMANCE_NONE = 0,
    /** 低能耗模式 */
    OH_NN_PERFORMANCE_LOW = 1,
    /** 中性能模式 */
    OH_NN_PERFORMANCE_MEDIUM = 2,
    /** 高性能模式 */
    OH_NN_PERFORMANCE_HIGH = 3,
    /** 极致性能模式 */
    OH_NN_PERFORMANCE_EXTREME = 4
} OH_NN_PerformanceMode;

/**
 * @brief 模型推理任务优先级。
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** 无优先级偏好 */
    OH_NN_PRIORITY_NONE = 0,
    /** 低优先级 */
    OH_NN_PRIORITY_LOW = 1,
    /** 中优先级 */
    OH_NN_PRIORITY_MEDIUM = 2,
    /** 高优先级 */
    OH_NN_PRIORITY_HIGH = 3
} OH_NN_Priority;

/**
 * @brief Neural Network Runtime 定义的错误码类型。
 *
 * @since 9
 * @version 2.0
 */
typedef enum {
    /** 操作成功 */
    OH_NN_SUCCESS = 0,
    /** 操作失败 */
    OH_NN_FAILED = 1,
    /** 非法参数 */
    OH_NN_INVALID_PARAMETER = 2,
    /** 内存相关的错误，包括：内存不足、内存数据拷贝失败、内存申请失败等。 */
    OH_NN_MEMORY_ERROR = 3,
    /** 非法操作 */
    OH_NN_OPERATION_FORBIDDEN = 4,
    /** 空指针异常 */
    OH_NN_NULL_PTR = 5,
    /** 无效文件 */
    OH_NN_INVALID_FILE = 6,
    /** 硬件发生错误，错误可能包含：HDL服务崩溃 
     * @deprecated since 11
     * @useinstead {@link OH_NN_UNAVAILABLE_DEVICE}
     */
    OH_NN_UNAVALIDABLE_DEVICE = 7,
    /** 非法路径 */
    OH_NN_INVALID_PATH = 8,
    /** 执行超时 
     * @since 11
     */
    OH_NN_TIMEOUT = 9,
    /** 未支持 
     * @since 11
     */
    OH_NN_UNSUPPORTED = 10,
    /** 连接异常 
     * @since 11
     */
    OH_NN_CONNECTION_EXCEPTION = 11,
    /** 保存cache异常
     * @since 11
     */
    OH_NN_SAVE_CACHE_EXCEPTION = 12,
    /** 动态shape
     * @since 11
     */
    OH_NN_DYNAMIC_SHAPE = 13,
    /** 硬件发生错误，错误可能包含：HDL服务崩溃 
     * @since 11
     */
    OH_NN_UNAVAILABLE_DEVICE = 14
} OH_NN_ReturnCode;

/**
 * @brief 异步推理结束后的回调处理函数句柄。
 * 
 * 使用参数<b>userData</b>来查询希望获取的那次异步推理执行。<b>userData</b>与调用异步推理{@link OH_NNExecutor_RunAsync}接口时
 * 传入的参数<b>userData</b>是一致的。使用参数<b>errCode</b>（{@link OH_NN_ReturnCode}类型）来获取该次异步推理的返回状态。 \n
 * 
 * @param userData 异步推理执行的标识符，与调用异步推理{@link OH_NNExecutor_RunAsync}接口时传入的参数<b>userData</b>一致。
 * @param errCode 该次异步推理的返回状态（{@link OH_NN_ReturnCode}类型）。
 * @param outputTensor 异步推理的输出张量，与调用异步推理{@link OH_NNExecutor_RunAsync}接口时传入的参数<b>outputTensor</b>一致。
 * @param outputCount 异步推理输出张量的数量，与调用异步推理{@link OH_NNExecutor_RunAsync}接口时传入的参数<b>outputCount</b>一致。
 * @since 11
 * @version 1.0
 */
typedef void (*NN_OnRunDone)(void *userData, OH_NN_ReturnCode errCode, void *outputTensor[], int32_t outputCount);

/**
 * @brief 异步推理执行期间设备驱动服务异常终止时的回调处理函数句柄。
 * 
 * 如果该回调函数被调用，您需要重新编译模型。 \n
 * 
 * 使用参数<b>userData</b>来查询希望获取的那次异步推理执行。<b>userData</b>与调用异步推理{@link OH_NNExecutor_RunAsync}接口时
 * 传入的参数<b>userData</b>是一致的。 \n
 * 
 * @param userData 异步推理执行的标识符，与调用异步推理{@link OH_NNExecutor_RunAsync}接口时传入的参数<b>userData</b>一致。 
 * @since 11
 * @version 1.0
 */
typedef void (*NN_OnServiceDied)(void *userData);

/**
 * @brief Neural Network Runtime 融合算子中激活函数的类型。
 *
 * @since 9
 * @version 1.0
 */
typedef enum : int8_t {
    /** 未指定融合激活函数 */
    OH_NN_FUSED_NONE = 0,
    /** 融合relu激活函数 */
    OH_NN_FUSED_RELU = 1,
    /** 融合relu6激活函数 */
    OH_NN_FUSED_RELU6 = 2
} OH_NN_FuseType;

/**
 * @brief 张量数据的排布类型。
 *
 * @since 9
 * @version 2.0
 */
typedef enum {
    /** 当张量没有特定的排布类型时（如标量或矢量），使用本枚举值 */
    OH_NN_FORMAT_NONE = 0,
    /** 当张量按照NCHW的格式排布数据时，使用本枚举值 */
    OH_NN_FORMAT_NCHW = 1,
    /** 当张量按照NHWC的格式排布数据时，使用本枚举值 */
    OH_NN_FORMAT_NHWC = 2,
    /** 当张量按照ND的格式排布数据时，使用本枚举值
     * @since 11
     */
    OH_NN_FORMAT_ND = 3
} OH_NN_Format;

/**
 * @brief Neural Network Runtime 支持的设备类型。
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** 不属于CPU、GPU、专用加速器的设备 */
    OH_NN_OTHERS = 0,
    /** CPU设备 */
    OH_NN_CPU = 1,
    /** GPU设备 */
    OH_NN_GPU = 2,
    /** 专用硬件加速器 */
    OH_NN_ACCELERATOR = 3,
} OH_NN_DeviceType;

/**
 * @brief Neural Network Runtime 支持的数据类型。
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** 张量数据类型未知 */
    OH_NN_UNKNOWN = 0,
    /** 张量数据类型为bool */
    OH_NN_BOOL = 1,
    /** 张量数据类型为int8 */
    OH_NN_INT8 = 2,
    /** 张量数据类型为int16 */
    OH_NN_INT16 = 3,
    /** 张量数据类型为int32 */
    OH_NN_INT32 = 4,
    /** 张量数据类型为int64 */
    OH_NN_INT64 = 5,
    /** 张量数据类型为uint8 */
    OH_NN_UINT8 = 6,
    /** 张量数据类型为uint16 */
    OH_NN_UINT16 = 7,
    /** 张量数据类型为uint32 */
    OH_NN_UINT32 = 8,
    /** 张量数据类型为uint64 */
    OH_NN_UINT64 = 9,
    /** 张量数据类型为float16 */
    OH_NN_FLOAT16 = 10,
    /** 张量数据类型为float32 */
    OH_NN_FLOAT32 = 11,
    /** 张量数据类型为float64 */
    OH_NN_FLOAT64 = 12
} OH_NN_DataType;


/**
 * @brief Neural Network Runtime 支持算子的类型。
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /**
     * 返回两个输入张量对应元素相加的和的张量。
     *
     * 输入：
     *
     * * input1，第一个输入的张量，数据类型要求为布尔值或者数字。
     * * input2，第二个输入的张量，数据类型和形状需要和第一个输入保持一致。
     *
     * 参数：
     *
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，input1和input2的和，数据形状与输入broadcast之后一样，数据类型与较高精度的输入精度一致
     */
    OH_NN_OPS_ADD = 1,

    /**
     * 在输入张量上应用 2D 平均池化，仅支持NHWC格式的张量。支持int8量化输入。
     *
     * 如果输入中含有padMode参数：
     *
     * 输入：
     *
     * * input，一个张量。
     *
     * 参数：
     *
     * * kernelSize，用来取平均值的kernel大小，是一个长度为2的int数组[kernel_height，kernel_weight]，
     *      第一个数表示kernel高度，第二个数表示kernel宽度。
     * * strides，kernel移动的距离，是一个长度为2的int数组[stride_height，stride_weight]，
     *      第一个数表示高度上的移动步幅，第二个数表示宽度上的移动步幅。
     * * padMode，填充模式，int类型的可选值，0表示same，1表示valid，并且以最近邻的值填充。
     *      same，输出的高度和宽度与input相同，填充总数将在水平和垂直方向计算，并在可能的情况下均匀分布到顶部
     *      和底部、左侧和右侧。否则，最后一个额外的填充将从底部和右侧完成。
     *      valid，输出的可能最大高度和宽度将在不填充的情况下返回。额外的像素将被丢弃。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 如果输入中含有padList参数：
     *
     * 输入：
     *
     * * input，一个张量。
     *
     * 参数：
     *
     * * kernelSize，用来取平均值的kernel大小，是一个长度为2的int数组[kernel_height，kernel_weight]，
     *      第一个数表示kernel高度，第二个数表示kernel宽度。
     * * strides，kernel移动的距离，是一个长度为2的int数组[stride_height，stride_weight]，
     *      第一个数表示高度上的移动步幅，第二个数表示宽度上的移动步幅。
     * * padList，input周围的填充，是一个长度为4的int数组[top，bottom，left，right]，并且以最近邻的值填充。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，对input进行平均池化后的结果。
     */
    OH_NN_OPS_AVG_POOL = 2,

    /**
     * 对一个张量进行batch normalization，对张量元素进行缩放和位移，缓解一批数据中潜在的covariate shift。
     *
     * 输入：
     *
     * * input，一个n维的张量，要求形状为[N，...，C]，即第n维是通道数（channel）。
     * * scale，缩放因子的1D张量，用于缩放归一化的第一个张量。
     * * offset，用于偏移的1D张量，以移动到归一化的第一个张量。
     * * mean，总体均值的一维张量，仅用于推理；对于训练，必须为空。
     * * variance，用于总体方差的一维张量。仅用于推理；对于训练，必须为空。
     *
     * 参数：
     *
     * * epsilon，数值稳定性的小附加值。
     *
     * 输出：
     *
     * * output，n维输出张量，其形状和数据类型与input一致。
     */
    OH_NN_OPS_BATCH_NORM = 3,

    /**
     * 将一个4维张量的batch维度按block_shape切分成小块，并将这些小块拼接到空间维度。
     *
     * 参数：
     *
     * * input，输入张量，维将被切分，拼接回空间维度。
     *
     * 输出：
     *
     * * blockSize，一个长度为2的数组[height_block，weight_block]，指定切分到空间维度上的block大小。
     * * crops，一个shape为(2，2)的二维数组[[crop0_start，crop0_end]，[crop1_start，crop1_end]]，
     *      表示在output的空间维度上截掉部分元素。
     *
     * 输出：
     *
     * * output，假设input的形状为(n，h，w，c)，output的形状为（n'，h'，w'，c'）：
     *      n' = n / (block_shape[0] * block_shape[1])
     *      h' = h * block_shape[0] - crops[0][0] - crops[0][1]
     *      w' = w * block_shape[1] - crops[1][0] - crops[1][1]
     *      c'= c
     */
    OH_NN_OPS_BATCH_TO_SPACE_ND = 4,

    /**
     * 对给出的输入张量上的各个维度方向上的数据进行偏置。
     *
     * 输入：
     *
     * * input，输入张量，可为2-5维度。
     * * bias，参数对应输入维度数量的偏移值。
     *
     * 输出：
     *
     * * output，根据输入中每个维度方向偏移后的结果。
     */
    OH_NN_OPS_BIAS_ADD = 5,

    /**
     * 对输入张量中的数据类型进行转换。
     *
     * 输入：
     *
     * * input，输入张量。
     * * type，转换后的数据类型。
     *
     * 输出：
     *
     * * output，转换后的张量。
     */
    OH_NN_OPS_CAST = 6,

    /**
     * 在指定维度上连接张量。
     *
     * 输入：
     *
     * * input：N个输入张量。
     *
     * 参数：
     *
     * * axis，指定。张量连接的维度
     *
     * 输出：
     *
     * * output，输出N个张量沿axis连接的结果。
     */
    OH_NN_OPS_CONCAT = 7,

    /**
     * 二维卷积层。
     *
     * 如果输入中含有padMode参数：
     *
     * 输入：
     *
     * * input，输入张量。
     * * weight，卷积的权重，要求weight排布为[outChannel，kernelHeight，kernelWidth，inChannel/group]，
     *      inChannel必须要能整除group。
     * * bias，卷积的偏置，是长度为[outChannel]的数组。在量化场景下，bias参数不需要量化参数，其量化
     *      版本要求输入 OH_NN_INT32类型数据，实际量化参数由input和weight共同决定。
     *
     * 参数：
     *
     * * stride，卷积核在height和weight上的步幅，是一个长度为2的int数组[strideHeight，strideWidth]。
     * * dilation，表示扩张卷积在height和weight上的扩张率，是一个长度为2的int数组[dilationHeight，dilationWidth]，
     *      值必须大于或等于1，并且不能超过input的height和width。
     * * padMode，input的填充模式，支持same和valid，int类型，0表示same，1表示valid。
     *      same，输出的高度和宽度与input相同，填充总数将在水平和垂直方向计算，并在可能的情况下均匀分布到顶部和底部、左侧
     *      和右侧。否则，最后一个额外的填充将从底部和右侧完成。
     *      Valid，输出的可能最大高度和宽度将在不填充的情况下返回。额外的像素将被丢弃。
     * * group，将input按in_channel分组，int类型。group等于1，这是常规卷积；group大于1且小于或等于in_channel，这是分组卷积。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     *
     * 如果输入中含有padList参数：
     *
     * 输入：
     *
     * * input，输入张量。
     * * weight，卷积的权重，要求weight排布为[outChannel，kernelHeight，kernelWidth，inChannel/group]，
     *      inChannel必须要能整除group。
     * * bias，卷积的偏置，是长度为[outChannel]的数组。在量化场景下，bias 参数不需要量化参数，其量化
     *      版本要求输入 OH_NN_INT32类型数据，实际量化参数由input和weight共同决定。
     *
     * 参数：
     *
     * * stride，卷积核在height和weight上的步幅，是一个长度为2的int数组[strideHeight，strideWidth]。
     * * dilation，表示扩张卷积在height和weight上的扩张率，是一个长度为2的int数组[dilationHeight，dilationWidth]。
     *      值必须大于或等于1，并且不能超过input的height和width。
     * * padList，input周围的填充，是一个长度为4的int数组[top，bottom，left，right]。
     * * group，将input按in_channel分组，int类型。
     *      group等于1，这是常规卷积。
     *      group等于in_channel，这是depthwiseConv2d，此时group==in_channel==out_channel。
     *      group大于1且小于in_channel，这是分组卷积，out_channel==group。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，卷积计算结果。
     */
    OH_NN_OPS_CONV2D = 8,

    /**
     * 二维卷积转置。
     *
     * 如果输入中含有padMode参数：
     *
     * 输入：
     *
     * * input，输入张量。
     * * weight，卷积的权重，要求weight排布为[outChannel，kernelHeight，kernelWidth，inChannel/group]，
     *      inChannel必须要能整除group。
     * * bias，卷积的偏置，是长度为[outChannel]的数组。在量化场景下，bias 参数不需要量化参数，其量化
     *      版本要求输入 OH_NN_INT32 类型数据，实际量化参数由input和weight共同决定。
     * * stride，卷积核在height和weight上的步幅，是一个长度为2的int数组[strideHeight，strideWidth]。
     *
     * 参数：
     *
     * * dilation，表示扩张卷积在height和weight上的扩张率，是一个长度为2的int数组[dilationHeight，dilationWidth]。
     *      值必须大于或等于1，并且不能超过input的height和width。
     * * padMode，input的填充模式，支持same和valid，int类型，0表示same，1表示valid。
     *      same，输出的高度和宽度与input相同，填充总数将在水平和垂直方向计算，并在可能的情况下均匀分布到顶部和底部、左侧
     *      和右侧。否则，最后一个额外的填充将从底部和右侧完成。
     *      Valid，输出的可能最大高度和宽度将在不填充的情况下返回。额外的像素将被丢弃。
     * * group，将input按in_channel分组，int类型。group等于1，这是常规卷积；group大于1且小于或等于in_channel，这是分组卷积。
     * * outputPads，一个整数或元组/2 个整数的列表，指定沿输出张量的高度和宽度的填充量。可以是单个整数，用于为所
     *      有空间维度指定相同的值。沿给定维度的输出填充量必须小于沿同一维度的步幅。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 如果输入中含有padList参数：
     *
     * 输入：
     *
     * * input，输入张量。
     * * weight，卷积的权重，要求weight排布为[outChannel，kernelHeight，kernelWidth，inChannel/group]，
     *      inChannel必须要能整除group。
     * * bias，卷积的偏置，是长度为[outChannel]的数组。在量化场景下，bias 参数不需要量化参数，其量化
     *      版本要求输入 OH_NN_INT32类型数据，实际量化参数由input和weight共同决定。
     *
     * 参数：
     *
     * * stride，卷积核在height和weight上的步幅，是一个长度为2的int数组[strideHeight，strideWidth]。
     * * dilation，表示扩张卷积在height和weight上的扩张率，是一个长度为2的int数组[dilationHeight，dilationWidth]。
     *      值必须大于或等于1，并且不能超过input的height和width。
     * * padList，input周围的填充，是一个长度为4的int数组[top，bottom，left，right]。
     * * group，将input按in_channel分组，int类型。group等于1，这是常规卷积；group大于1且小于或等于in_channel，这是分组卷积。
     * * outputPads，一个整数或元组/2 个整数的列表，指定沿输出张量的高度和宽度的填充量。可以是单个整数，用于为所
     *      有空间维度指定相同的值。沿给定维度的输出填充量必须小于沿同一维度的步幅。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，卷积转置后的计算结果。
     */
    OH_NN_OPS_CONV2D_TRANSPOSE = 9,

    /**
     * 二维深度可分离卷积
     *
     * 如果输入中含有padMode参数：
     *
     * 输入：
     *
     * * input，输入张量。
     * * weight，卷积的权重，要求weight排布为[outChannel，kernelHeight，kernelWidth，1]，outChannel = channelMultiplier * inChannel。
     * * bias，卷积的偏置，是长度为[outChannel]的数组。在量化场景下，bias参数不需要量化参数，其量化
     *      版本要求输入 OH_NN_INT32类型数据，实际量化参数由input和weight共同决定。
     *
     * 参数：
     *
     * * stride，卷积核在height和weight上的步幅，是一个长度为2的int数组[strideHeight，strideWidth]。
     * * dilation，表示扩张卷积在height和weight上的扩张率，是一个长度为2的int数组[dilationHeight，dilationWidth]。
     *      值必须大于或等于1，并且不能超过input的height和width。
     * * padMode，input的填充模式，支持same和valid，int类型，0表示same，1表示valid
     *      same，输出的高度和宽度与input相同，填充总数将在水平和垂直方向计算，并在可能的情况下均匀分布到顶部和底部、左侧
     *      和右侧。否则，最后一个额外的填充将从底部和右侧完成
     *      Valid，输出的可能最大高度和宽度将在不填充的情况下返回。额外的像素将被丢弃
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 如果输入中含有padList 参数：
     *
     * 输入：
     *
     * * input，输入张量。
     * * weight，卷积的权重，要求weight排布为[outChannel，kernelHeight，kernelWidth，1]，outChannel = channelMultiplier * inChannel。
     * * bias，卷积的偏置，是长度为[outChannel]的数组。在量化场景下，bias 参数不需要量化参数，其量化
     *      版本要求输入 OH_NN_INT32 类型数据，实际量化参数由input和weight共同决定。
     *
     * 参数：
     *
     * * stride，卷积核在height和weight上的步幅，是一个长度为2的int数组[strideHeight，strideWidth]。
     * * dilation，表示扩张卷积在height和weight上的扩张率，是一个长度为2的int数组[dilationHeight，dilationWidth]。
     *      值必须大于或等于1，并且不能超过input的height和width。
     * * padList，input周围的填充，是一个长度为4的int数组[top，bottom，left，right]。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，卷积计算的结果。
     */
    OH_NN_OPS_DEPTHWISE_CONV2D_NATIVE = 10,

    /**
     * 对输入的两个标量或张量做除法。
     *
     * 输入：
     *
     * * input1，第一个输入是标量或布尔值或数据类型为数字或布尔值的张量。
     * * input2，数据类型根据input1的类型，要求有所不同：
     *      当第一个输入是张量时，第二个输入可以是实数或布尔值或数据类型为实数/布尔值的张量。
     *      当第一个输入是实数或布尔值时，第二个输入必须是数据类型为实数/布尔值的张量。
     *
     * 参数：
     *
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，input1和input2相除的结果。
     */
    OH_NN_OPS_DIV = 11,

    /**
     * 设置参数对输入进行product(点乘)、sum(相加减)或max(取大值)。
     *
     * 输入：
     *
     * * input1，第一个输入张量。
     * * input2，第二个输入张量。
     *
     * 参数：
     *
     * * mode，枚举，选择操作方式。
     *
     * 输出：
     *
     * * output，计算后的结果，output和input1拥有相同的数据类型和形状。
     *
     */
    OH_NN_OPS_ELTWISE = 12,

    /**
     * 在给定维度上为张量添加一个额外的维度。
     *
     * 输入：
     *
     * * input，输入张量。
     * * axis，需要添加的维度的index，int32_t类型，值必须在[-dim-1，dim]，且只允许常量值。
     *
     * 输出：
     *
     * * output，维度拓展后的张量。
     */
    OH_NN_OPS_EXPAND_DIMS = 13,

    /**
     * 根据指定的维度，创建由一个标量填充的张量。
     *
     * 输入：
     *
     * * value，填充的标量。
     * * shape，指定创建张量的形状。
     *
     * 输出：
     *
     * * output，生成的张量，和value具有相同的数据类型，张量形状有shape参数指定。
     */
    OH_NN_OPS_FILL = 14,

    /**
     * 全连接，整个输入作为feature map，进行特征提取。
     *
     * 输入：
     *
     * * input，全连接的输入张量。
     * * weight，全连接的权重张量。
     * * bias，全连接的偏置，在量化场景下，bias 参数不需要量化参数，其量化
     *      版本要求输入OH_NN_INT32类型数据，实际量化参数由input和weight共同决定。
     *
     * 参数：
     *
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，输出运算后的张量。

     * 如果输入中含有axis参数：
     *
     * 输入：
     *
     * * input，全连接的输入张量。
     * * weight，全连接的权重张量。
     * * bias，全连接的偏置，在量化场景下，bias 参数不需要量化参数，其量化
     *      版本要求输入 OH_NN_INT32 类型数据，实际量化参数由input和weight共同决定。
     *
     * 参数：
     *
     * * axis，input做全连接的轴，从指定轴axis开始，将axis和axis后面的轴展开成一维去做全连接。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，输出运算后的张量。
     */
    OH_NN_OPS_FULL_CONNECTION = 15,

    /**
     * 根据指定的索引和轴返回输入张量的切片。
     *
     * 输入：
     *
     * * input，输入待切片的张量。
     * * inputIndices，指定input在axis上的索引，是一个int类型的数组，值必须在[0,input.shape[axis])范围内
     * * axis，input被切片的轴，int32_t类型的数组，数组长度为1。
     *
     * 输出：
     *
     * * output，输出切片后的张量。
     */
    OH_NN_OPS_GATHER = 16,

    /**
     * 计算输入的Hswish激活值。
     *
     * 输入：
     *
     * * 一个n维输入张量。
     *
     * 输出：
     *
     * * n维Hswish激活值，数据类型和shape和input一致。
     */
    OH_NN_OPS_HSWISH = 17,

    /**
     * 对input1和input2，计算每对元素的input1[i]<=input2[i]的结果，i是输入张量中每个元素的索引。
     *
     * 输入：
     *
     * *  input1，可以是实数、布尔值或数据类型是实数/NN_BOOL的张量。
     * *  input2，如果input_1是张量，input_2可以是实数、布尔值，否则只能是张量，其数据类型是实数或NN_BOOL。
     *
     * 输出：
     *
     * * 张量，数据类型为NN_BOOL的张量，使用量化模型时，output的量化参数不可省略，但量化参数的数值不会对输入结果产生影响。
     */
    OH_NN_OPS_LESS_EQUAL = 18,

    /**
     * 计算input1和input2的内积
     *
     * 输入：
     *
     * * input1，n维输入张量。
     * * input2，n维输入张量。
     *
     * 参数：
     *
     * * TransposeX，布尔值，是否对input1进行转置。
     * * TransposeY，布尔值，是否对input2进行转置。
     *
     * 输出：
     *
     * * output，计算得到内积，当type!=NN_UNKNOWN时，output数据类型由type决定；当type==NN_UNKNOWN时，
     *      output的数据类型取决于inputX和inputY进行计算时转化的数据类型。
     */
    OH_NN_OPS_MATMUL = 19,

    /**
     * 计算input1和input2对应元素最大值，input1和input2的输入遵守隐式类型转换规则，使数据类型一致。输入必须
     * 是两个张量或一个张量和一个标量。当输入是两个张量时，它们的数据类型不能同时为NN_BOOL。它们的形状支持
     * broadcast成相同的大小。当输入是一个张量和一个标量时，标量只能是一个常数。
     *
     * 输入：
     *
     * *  input1，n维输入张量，实数或NN_BOOL类型。
     * *  input2，n维输入张量，实数或NN_BOOL类型。
     *
     * 输出：
     *
     * * output，n维输出张量，output的shape和数据类型和两个input中精度或者位数高的相同。
     */
    OH_NN_OPS_MAXIMUM = 20,

    /**
     * 在输入张量上应用 2D 最大值池化。
     *
     * 如果输入中含有padMode参数：
     *
     * 输入：
     *
     * * input，一个张量。
     *
     * 参数：
     *
     * * kernelSize，用来取最大值的kernel大小，是一个长度为2的int数组[kernel_height，kernel_weight]，
     *       第一个数表示kernel高度，第二个数表示kernel宽度。
     * * strides，kernel移动的距离，是一个长度为2的int数组[stride_height，stride_weight]，
     *      第一个数表示高度上的移动步幅，第二个数表示宽度上的移动步幅。
     * * padMode，填充模式，int类型的可选值，0表示same，1表示valid，并且以最近邻的值填充。
     *      same，输出的高度和宽度与input相同，填充总数将在水平和垂直方向计算，并在可能的情况下均匀分布到顶部
     *      和底部、左侧和右侧。否则，最后一个额外的填充将从底部和右侧完成。
     *      valid，输出的可能最大高度和宽度将在不填充的情况下返回。额外的像素将被丢弃。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 如果输入中含有padList参数：
     *
     * 输入：
     *
     * * input，一个张量。
     *
     * 参数：
     *
     * * kernelSize，用来取最大值的kernel大小，是一个长度为2的int数组[kernel_height，kernel_weight]，
     *       第一个数表示kernel高度，第二个数表示kernel宽度。
     * * strides，kernel移动的距离，是一个长度为2的int数组[stride_height，stride_weight]，
     *      第一个数表示高度上的移动步幅，第二个数表示宽度上的移动步幅。
     * * padList，input周围的填充，是一个长度为4的int数组[top，bottom，left，right]，并且以最近邻的值填充。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，对input最大值池化后的张量。
     */
    OH_NN_OPS_MAX_POOL = 21,

    /**
     * 将inputX和inputY相同的位置的元素相乘得到output。如果inputX和inputY类型shape不同，要求inputX和inputY可以
     * 通过broadcast扩充成相同的shape进行相乘。
     *
     * 输入：
     *
     * * input1，一个n维张量。
     * * input2，一个n维张量。
     *
     * 参数：
     *
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，input1和input2每个元素的乘积。
     */
    OH_NN_OPS_MUL = 22,

    /**
     * 根据indices指定的位置，生成一个由one-hot向量构成的张量。每个onehot向量中的有效值由on_value决定，其他位置由off_value决定。
     *
     * 输入：
     *
     * *  indices，n维张量。indices中每个元素决定每个one-hot向量，on_value的位置
     * *  depth，一个整型标量，决定one-hot向量的深度。要求depth>0。
     * *  on_value，一个标量，指定one-hot向量中的有效值。
     * *  off_value，(一个标量，指定one-hot向量中除有效位以外，其他位置的值。
     *
     * 参数：
     *
     * *  axis，一个整型标量，指定插入one-hot的维度。
     *       indices的形状是[N，C]，depth的值是D，当axis=0时，output形状为[D，N，C]，
     *       indices的形状是[N，C]，depth的值是D，当axis=-1时，output形状为[N，C，D]，
     *       indices的形状是[N，C]，depth的值是D，当axis=1时，output形状为[N，D，C]。
     *
     * 输出：
     *
     * * output，如果indices时n维张量，则output是(n+1)维张量。output的形状由indices和axis共同决定。
     */
    OH_NN_OPS_ONE_HOT = 23,

    /**
     * 在inputX指定维度的数据前后，添加指定数值进行增广。
     *
     * 输入：
     *
     * * inputX，一个n维张量，要求inputX的排布为[BatchSize，…]。
     * * paddings，一个二维张量，指定每一维度增补的长度，shape为[n，2]。paddings[i][0]表示第i维上，需要在inputX前增补的数量；
     *      paddings[i][1]表示第i维上，需要在inputX后增补的数量。
     *
     * 参数：
     *
     * * padValues，一个常数，数据类型和inputX一致，指定Pad操作补全的数值。
     *
     * 输出：
     *
     * * output，一个n维张量，维数和数据类型和inputX保持一致。shape由inputX和paddings共同决定
     *      output.shape[i] = input.shape[i] + paddings[i][0]+paddings[i][1]。
     */
    OH_NN_OPS_PAD = 24,

    /**
     * 求input的y次幂，输入必须是两个张量或一个张量和一个标量。当输入是两个张量时，它们的数据类型不能同时为NN_BOOL，
     * 且要求两个张量的shape相同。当输入是一个张量和一个标量时，标量只能是一个常数。
     *
     * 输入：
     *
     * * input，实数、bool值或张量，张量的数据类型为实数/NN_BOOL。
     * * y，实数、bool值或张量，张量的数据类型为实数/NN_BOOL。
     *
     * 输出：
     *
     * * output，形状由input和y broadcast后的形状决定。
     */
    OH_NN_OPS_POW = 25,

    /**
     * 给定一个张量，计算其缩放后的值。
     *
     * 输入：
     *
     * * input，一个n维张量。
     * * scale，缩放张量。
     * * bias，偏置张量。
     *
     * 参数：
     *
     * * axis，指定缩放的维度。
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，scale的计算结果，一个n维张量，类型和input一致，shape由axis决定。
     */
    OH_NN_OPS_SCALE = 26,

    /**
     * 输入一个张量，计算其shape。
     *
     * 输入：
     *
     * * input，一个n维张量。
     *
     * 输出：
     *
     * * output，输出张量的维度，一个整型数组。
     */
    OH_NN_OPS_SHAPE = 27,

    /**
     * 给定一个张量，计算其sigmoid结果。
     *
     * 输入：
     *
     * * input，一个n维张量。
     *
     * 输出：
     *
     * * output，sigmoid的计算结果，一个n维张量，类型和shape和input一致。
     */
    OH_NN_OPS_SIGMOID = 28,

    /**
     * 在input 张量各维度，以begin为起点，截取size长度的切片。
     *
     * 输入：
     *
     * * input，n维输入张量。
     * * begin，一组不小于0的整数，指定每个维度上的起始切分点。
     * * size，一组不小于1的整数，指定每个维度上切片的长度。假设某一维度i，1<=size[i]<=input.shape[i]-begin[i]。
     *
     * 输出：
     *
     * * output，切片得到的n维张量，其TensorType和input一致，shape和size相同。
     */
    OH_NN_OPS_SLICE = 29,

    /**
     * 给定一个张量，计算其softmax结果。
     *
     * 输入：
     *
     * * input，n维输入张量。
     *
     * 参数：
     *
     * * axis，int64类型，指定计算softmax的维度。整数取值范围为[-n，n)。
     *
     * 输出：
     *
     * * output，softmax的计算结果，一个n维张量，类型和shape和input一致。
     */
    OH_NN_OPS_SOFTMAX = 30,

    /**
     * 将4维张量在空间维度上进行切分成blockShape[0] * blockShape[1]个小块，然后在batch维度上拼接这些小块。
     *
     * 输入：
     *
     * * input，一个四维张量
     *
     * 参数：
     *
     * * blockShape，一对整数，每个整数不小于1。
     * * paddings，一对数组，每个数组由两个整数组成。组成paddings的4个整数都不小于0。paddings[0][0]和paddings[0][1]指
     *      定了第三个维度上padding的数量，paddings[1][0]和paddings[1][1]指定了第四个维度上padding的数量。
     *
     * 输出：
     *
     * * output，一个4维张量，数据类型和input一致。shape由input，blockShape和paddings共同决定，假设input shape为[n，c，h，w]，则有
     *      output.shape[0] = n * blockShape[0] * blockShape[1]
     *      output.shape[1] = c
     *      output.shape[2] = (h + paddings[0][0] + paddings[0][1]) / blockShape[0]
     *      output.shape[3] = (w + paddings[1][0] + paddings[1][1]) / blockShape[1]
     *      要求(h + paddings[0][0] + paddings[0][1])和(w + paddings[1][0] + paddings[1][1])能被
     *      blockShape[0]和blockShape[1]整除。
     */
    OH_NN_OPS_SPACE_TO_BATCH_ND = 31,

    /**
     * Split 算子沿 axis 维度将 input 拆分成多个 张量，张量 数量由 outputNum 指定。
     *
     * 输入：
     *
     * * input，n维张量。
     *
     * 参数：
     *
     * * outputNum，long，输出张量的数量，output_num类型为int。
     * * size_splits，一维张量，指定 张量 沿 axis 轴拆分后，每个 张量 的大小，size_splits 类型为 int。
     *      如果 size_splits 的数据为空，则 张量 被拆分成大小均等的 张量，此时要求 input.shape[axis] 可以被 outputNum 整除；
     *      如果 size_splits 不为空，则要求 size_splits 所有元素之和等于 input.shape[axis]。
     * * axis，指定拆分的维度，axis类型为int。
     *
     * 输出：
     *
     * * outputs，一组n维张量，每一个张量类型和shape相同，每个张量的类型和input一致。
     */
    OH_NN_OPS_SPLIT = 32,

    /**
     * 给定一个张量，计算其平方根。
     *
     * 输入：
     *
     * * input，一个n维张量。
     *
     * 输出：
     *
     * * output，输入的平方根，一个n维张量，类型和shape和input一致。
     */
    OH_NN_OPS_SQRT = 33,

    /**
     * 计算两个输入的差值并返回差值的平方。SquaredDifference算子支持张量和张量相减。
     * 如果两个张量的TensorType不相同，Sub算子会将低精度的张量转成更高精度的类型。
     * 如果两个张量的shape不同，要求两个张量可以通过broadcast拓展成相同shape的张量。
     *
     * 输入：
     *
     * * input1，被减数，input1是一个张量，张量的类型可以是NN_FLOAT16、NN_FLOAT32、NN_INT32或NN_BOOL。
     * * input2，减数，input2是一个张量，张量的类型可以是NN_FLOAT16、NN_FLOAT32、NN_INT32或NN_BOOL。
     *
     * 输出：
     *
     * * output，两个输入差值的平方。output的shape由input1和input2共同决定，input1和input2的shape相同时，
     *      output的shape和input1、input2相同；shape不同时，需要将input1或input2做broadcast操作后，相减得到output。
     *      output的TensorType由两个输入中更高精度的TensorType决定。
     */
    OH_NN_OPS_SQUARED_DIFFERENCE = 34,

    /**
     * 去除axis中，长度为1的维度。支持int8量化输入假设input的shape为[2，1，1，2，2]，axis为[0,1]，
     * 则output的shape为[2，1，2，2]。第0维到第一维之间，长度为0的维度被去除。
     *
     * 输入：
     *
     * * input，n维张量。
     *
     * 参数：
     *
     * * axis，指定删除的维度。axis可以是一个int64_t的整数或数组，整数的取值范围为[-n，n)。
     *
     * 输出：
     *
     * * output，输出张量。
     */
    OH_NN_OPS_SQUEEZE = 35,

    /**
     * 将一组张量沿axis维度进行堆叠，堆叠前每个张量的维数为n，则堆叠后output维数为n+1。
     *
     * 输入：
     *
     * * input，Stack支持传入多个输入n维张量，每个张量要求shape相同且类型相同。
     *
     * 参数：
     *
     * * axis，一个整数，指定张量堆叠的维度。axis可以是负数，axis取值范围为[-(n+1)，(n+1))。
     *
     * 输出：
     *
     * * output，将input沿axis维度堆叠的输出，n+一维张量，TensorType和input相同。
     */
    OH_NN_OPS_STACK = 36,

    /**
     * 跨步截取张量
     *
     * 输入：
     *
     * * input，n维输入张量。
     * * begin，一维张量，begin的长度等于n，begin[i]指定第i维上截取的起点。
     * * end，一维张量，end的长度等于n，end[i]指定第i维上截取的终点。
     * * strides，一维张量，strides的长度等于n，strides[i]指定第i维上截取的步长。
     *
     * 参数：
     *
     * * beginMask，一个整数，用于解除begin的限制。将beginMask转成二进制表示，如果binary(beginMask)[i]==1，
     *      则对于第i维，从第一个元素开始，以strides[i]为步长截取元素直到第end[i]-1个元素。
     * * endMask，个整数，用于解除end的限制。将endMask转成二进制表示，如果binary(endMask)[i]==1，则对于第i维，
     *      从第begin[i]个元素起，以strides[i]为步长截取元素直到张量边界。
     * * ellipsisMask，一个整数，用于解除begin和end的限制。将ellipsisMask转成二进制表示，如果binary(ellipsisMask)[i]==1，
     *      则对于第i维，从第一个元素开始，以strides[i]为补偿，截取元素直到张量边界。binary(ellipsisMask)仅允许有一位不为0。
     * * newAxisMask，一个整数，用于新增维度。将newAxisMask转成二进制表示，如果binary(newAxisMask)[i]==1，则在第i维插入长度为1的新维度。
     * * shrinkAxisMask，一个整数，用于压缩指定维度。将shrinkAxisMask转成二进制表示，如果binary(shrinkAxisMask)[i]==1，
     *      则舍去第i维所有元素，第i维长度压缩至1。
     *
     * 输出：
     *
     * * 堆叠运算后的张量，数据类型与input相同。输出维度rank(input[0])+1 维。
     */
    OH_NN_OPS_STRIDED_SLICE = 37,

    /**
     * 计算两个输入的差值。
     *
     * 输入：
     *
     * * input1，被减数，input1是一个张量。
     * * input2，减数，input2是一个张量。
     *
     * 参数：
     *
     * * activationType，是一个整型常量，且必须是FuseType中含有的值。
     *      在输出之前调用指定的激活。
     *
     * 输出：
     *
     * * output，两个输入相减的差。output的shape由input1和input2共同决定，当input1和input2的shape相同时，output的shape和input1、input2相同；
     *      shape不同时，需要将input1或input2做broadcast操作后，相减得到output。output的TensorType由两个输入中更高精度的TensorType决定。
     */
    OH_NN_OPS_SUB = 38,

    /**
     * 计算输入张量的双曲正切值。
     *
     * 输入：
     *
     * * input，n维张量。
     *
     * 输出：
     *
     * * output，input的双曲正切，TensorType和张量 shape和input相同。
     */
    OH_NN_OPS_TANH = 39,

    /**
     * 以multiples指定的次数拷贝input。
     *
     * 输入：
     * * input，n维张量。
     * * multiples，一维张量，指定各个维度拷贝的次数。其长度m不小于input的维数n。
     *
     * 输出：
     * * 张量，m维张量，TensorType与input相同。如果input和multiples长度相同，
     *      则output和input维数一致，都是n维张量；如果multiples长度大于n，则用1填充input的维度，
     *      再在各个维度上拷贝相应的次数，得到m维张量。
     */
    OH_NN_OPS_TILE = 40,

    /**
     * 根据permutation对input 0进行数据重排。
     *
     * 输入：
     *
     * * input，n维张量，待重排的张量。
     * * perm，一维张量，其长度和input 0的维数一致。
     *
     * 输出：
     *
     * * output，n维张量，output 0的TensorType与input 0相同，shape由input 0的shape和permutation共同决定。
     */
    OH_NN_OPS_TRANSPOSE = 41,

    /**
     * keepDims为false时，计算指定维度上的平均值，减少input的维数；当keepDims为true时，计算指定维度上的平均值，保留相应的维度。
     *
     * 输入：
     *
     * *  input，n维输入张量，n<8。
     * *  axis，一维张量，指定计算均值的维度，axis中每个元素的取值范围为[-n，n)。
     *
     * 参数：
     *
     * *  keepDims，布尔值，是否保留维度的标志位。
     *
     * 输出：
     *
     * *  output，m维输出张量，数据类型和input相同。当keepDims为false时，m==n；当keepDims为true时，m<n。
     */
    OH_NN_OPS_REDUCE_MEAN = 42,

    /**
     * 采用Bilinear方法，按给定的参数对input进行变形。
     *
     * 输入：
     *
     * * input，四维输入张量，input中的每个元素不能小于0。input排布必须是[batchSize，height，width，channels]。
     *
     * 参数：
     *
     * * newHeight，resize之后4维张量的height值。
     * * newWidth，resize之后4维张量的width值。
     * * preserveAspectRatio，一个布尔值，指示resize操作是否保持input 张量的height/width比例。
     * * coordinateTransformMode，一个int32整数，指示Resize操作所使用的坐标变换方法，目前支持以下方法：
     * * excludeOutside，一个int64浮点数。当excludeOutside=1时，超出input边界的采样权重被置为0,其余权重重新归一化处理。
     *
     * 输出：
     *
     * * output，n维输出张量，output的shape和数据类型和input相同。
     */
    OH_NN_OPS_RESIZE_BILINEAR = 43,

     /**
     * 求input平方根的倒数。
     *
     * 输入：
     *
     * *  input，n维输入张量，input中的每个元素不能小于0，n<8。
     *
     * 输出：
     *
     * *  output，n维输出张量，output的shape和数据类型和input相同。

     */
    OH_NN_OPS_RSQRT = 44,

     /**
     * 根据inputShape调整input的形状。
     *
     * 输入：
     *
     * *  input，一个n维输入张量。
     * *  InputShape，一个一维张量，表示输出张量的shape，需要是一个常量张量。
     *
     * 输出：
     *
     * * output，输出张量，数据类型和input一致，shape由inputShape决定。
     */
    OH_NN_OPS_RESHAPE = 45,

    /**
     * 计算input和weight的PReLU激活值。
     *
     * 输入：
     *
     * *  input，一个n维张量，如果n>=2，则要求inputX的排布为[BatchSize，…，Channels]，第二个维度为通道数。
     * *  weight，一个一维张量。weight的长度只能是1或者等于通道数。当weight长度为1，则inputX所有通道共享一个权重值。
     *      若weight长度等于通道数，每个通道独享一个权重，若inputX维数n<2，weight长度只能为1。
     *
     * 输出：
     *
     *    output，input的PReLU激活值。形状和数据类型和inputX保持一致。
     */
    OH_NN_OPS_PRELU = 46,

    /**
     * 计算input的Relu激活值。
     *
     * 输入：
     *
     * * input，一个n维输入张量。
     *
     * 输出：
     *
     * * output，n维Relu输出张量，数据类型和shape和input一致。
     */
    OH_NN_OPS_RELU = 47,

    /**
     * 计算input的Relu6激活值，即对input中每个元素x，计算min(max(x，0)，6)。
     *
     * 输入：
     *
     * * input，一个n维输入张量。
     *
     * 输出：
     *
     * * output，n维Relu6输出张量，数据类型和shape和input一致。
     */
    OH_NN_OPS_RELU6 = 48,

    /**
     * 对一个张量从某一axis开始做层归一化。
     *
     * 输入：
     *
     * *  input，一个n维输入张量。
     * *  gamma，一个m维张量，gamma维度应该与input做归一化部分的shape一致。
     * *  beta，一个m维张量，shape与gamma一样。
     *
     * 参数：
     *
     * *  beginAxis，是一个NN_INT32的标量，指定开始做归一化的轴，取值范围是[1，rank(input))。
     * *  epsilon，是一个NN_FLOAT32的标量，是归一化公式中的微小量，常用值是1e-7。
     *
     * 输出：
     *
     * * output，n维输出张量，数据类型和shape和input一致。
     */
    OH_NN_OPS_LAYER_NORM = 49,

    /**
     * 沿着axis指定的维度，计算input的累积值。
     *
     * 输入：
     *
     * *  input，n维输入张量，n<8。
     * *  axis，一维张量，指定计算乘的维度，axis中每个元素的取值范围为[-n，n)。
     *
     * 参数：
     *
     * *  keepDims，布尔值，是否保留维度的标志位。当keepDIms为true时，output的维数和input保持一致；当keepDims为false时，
     *      output的维数缩减。
     *
     * 输出：
     *
     * *  output，m维输出张量，数据类型和input相同。当keepDims为false时，m==n；当keepDims为true时，m<n。
     */
    OH_NN_OPS_REDUCE_PROD = 50,

    /**
     * 当keepDims为false时，计算指定维度上的逻辑与，减少input的维数；当keepDims为true时，计算指定维度上的逻辑与，保留相应的维度。
     *
     * 输入：
     *
     * *  n维输入张量，n<8。
     * *  一维张量，指定计算逻辑与的维度，axis中每个元素的取值范围为[-n，n)。
     *
     * 参数：
     *
     * *  keepDims，布尔值，是否保留维度的标志位。
     *
     * 输出：
     * *  output，m维输出张量，数据类型和input相同。当keepDims为false时，m==n；当keepDims为true时，m<n。
     */
    OH_NN_OPS_REDUCE_ALL = 51,

    /**
     * 数据类型转换。
     *
     * 输入：
     *
     * *  input，n维张量。
     *
     * 参数：
     *
     * *  src_t，定义输入的数据类型。
     * *  dst_t，定义输出的数据类型。
     *
     * 输出：
     *
     * * output，n维张量，数据类型由input2决定  输出shape和输入相同。
     */
    OH_NN_OPS_QUANT_DTYPE_CAST = 52,

    /**
     * 查找沿最后一个维度的k个最大条目的值和索引。
     *
     * 输入：
     *
     * *  input，n维张量。
     * *  input k，指明是得到前k个数据以及其index。
     *
     * 参数：
     *
     * *  sorted，如果为True，按照大到小排序，如果为False，按照小到大排序。
     *
     * 输出：
     *
     * * output0,最后一维的每个切片中的k个最大元素。
     * * output1，输入的最后一个维度内的值的索引。
     */
    OH_NN_OPS_TOP_K = 53,

    /**
     * 返回跨轴的张量最大值的索引。
     *
     * 输入：
     *
     * *  input，n维张量，输入张量(N，∗)，其中∗意味着任意数量的附加维度。
     *
     * 参数：
     *
     * *  axis，指定求最大值索引的维度。
     * *  keep_dims，bool值，是否维持输入张量维度。
     *
     * 输出：
     * *  output，张量，轴上输入张量最大值的索引。
     */
    OH_NN_OPS_ARG_MAX = 54,

    /**
     * 根据输入axis的值。增加一个维度。
     *
     * 输入：
     * * input，n维张量。
     *
     * 参数：
     *
     * * axis，指定增加的维度。axis可以是一个整数或一组整数，整数的取值范围为[-n，n)。
     *
     * 输出：
     * * output，输出张量。
     */
    OH_NN_OPS_UNSQUEEZE = 55,

    /**
     * 高斯误差线性单元激活函数。output=0.5∗input∗(1+tanh(input/2))，不支持int量化输入。
     *
     * 输入：
     * * 一个n维输入张量。
     *
     * 输出：
     * * output，n维Relu输出张量，数据类型和shape和input一致。
     */
    OH_NN_OPS_GELU = 56,
} OH_NN_OperationType;

/**
 * @brief 张量的类型。
 *
 * 张量通常用于设置模型的输入、输出和算子参数。作为模型（或算子）的输入和输出时，需要将张量类型设置为{@link OH_NN_TENSOR}；当张量
 * 作为算子参数时，需要选择除{@link OH_NN_TENSOR}以外合适的枚举值，作为张量的类型。假设正在设置{@link OH_NN_OPS_CONV2D}
 * 算子的pad参数，则需要将{@link OH_NN_Tensor}实例的type属性设置为{@link OH_NN_CONV2D_PAD}。其他算子参数的设置以此类推，枚举值
 * 的命名遵守 OH_NN_{算子名称}_{属性名} 的格式。
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** 当张量作为模型（或算子）的输入或输出时，使用本枚举值 */
    OH_NN_TENSOR = 0,

    /** 当张量作为Add算子的activationType参数时，使用本枚举值 */
    OH_NN_ADD_ACTIVATIONTYPE = 1,

    /** 当张量作为AvgPool算子的kernel_size参数时，使用本枚举值 */
    OH_NN_AVG_POOL_KERNEL_SIZE = 2,
    /** 当张量作为AvgPool算子的stride参数时，使用本枚举值 */
    OH_NN_AVG_POOL_STRIDE = 3,
    /** 当张量作为AvgPool算子的pad_mode参数时，使用本枚举值 */
    OH_NN_AVG_POOL_PAD_MODE = 4,
    /** 当张量作为AvgPool算子的pad参数时，使用本枚举值 */
    OH_NN_AVG_POOL_PAD = 5,
    /** 当张量作为AvgPool算子的activation_type参数时，使用本枚举值 */
    OH_NN_AVG_POOL_ACTIVATION_TYPE = 6,

    /** 当张量作为BatchNorm算子的eosilon参数时，使用本枚举值 */
    OH_NN_BATCH_NORM_EPSILON = 7,

    /** 当张量作为BatchToSpaceND算子的blockSize参数时，使用本枚举值 */
    OH_NN_BATCH_TO_SPACE_ND_BLOCKSIZE = 8,
    /** 当张量作为BatchToSpaceND算子的crops参数时，使用本枚举值 */
    OH_NN_BATCH_TO_SPACE_ND_CROPS = 9,

    /** 当张量作为Concat算子的axis参数时，使用本枚举值 */
    OH_NN_CONCAT_AXIS = 10,

    /** 当张量作为Conv2D算子的strides参数时，使用本枚举值 */
    OH_NN_CONV2D_STRIDES = 11,
    /** 当张量作为Conv2D算子的pad参数时，使用本枚举值 */
    OH_NN_CONV2D_PAD = 12,
    /** 当张量作为Conv2D算子的dilation参数时，使用本枚举值 */
    OH_NN_CONV2D_DILATION = 13,
    /** 当张量作为Conv2D算子的padMode参数时，使用本枚举值 */
    OH_NN_CONV2D_PAD_MODE = 14,
    /** 当张量作为Conv2D算子的activationType参数时，使用本枚举值 */
    OH_NN_CONV2D_ACTIVATION_TYPE = 15,
    /** 当张量作为Conv2D算子的group参数时，使用本枚举值 */
    OH_NN_CONV2D_GROUP = 16,

    /** 当张量作为Conv2DTranspose算子的strides参数时，使用本枚举值 */
    OH_NN_CONV2D_TRANSPOSE_STRIDES = 17,
    /** 当张量作为Conv2DTranspose算子的pad参数时，使用本枚举值 */
    OH_NN_CONV2D_TRANSPOSE_PAD = 18,
    /** 当张量作为Conv2DTranspose算子的dilation参数时，使用本枚举值 */
    OH_NN_CONV2D_TRANSPOSE_DILATION = 19,
    /** 当张量作为Conv2DTranspose算子的outputPaddings参数时，使用本枚举值 */
    OH_NN_CONV2D_TRANSPOSE_OUTPUT_PADDINGS = 20,
    /** 当张量作为Conv2DTranspose算子的padMode参数时，使用本枚举值 */
    OH_NN_CONV2D_TRANSPOSE_PAD_MODE = 21,
    /** 当张量作为Conv2DTranspose算子的activationType参数时，使用本枚举值 */
    OH_NN_CONV2D_TRANSPOSE_ACTIVATION_TYPE = 22,
    /** 当张量作为Conv2DTranspose算子的group参数时，使用本枚举值 */
    OH_NN_CONV2D_TRANSPOSE_GROUP = 23,

    /** 当张量作为DepthwiseConv2dNative算子的strides参数时，使用本枚举值 */
    OH_NN_DEPTHWISE_CONV2D_NATIVE_STRIDES = 24,
    /** 当张量作为DepthwiseConv2dNative算子的pad参数时，使用本枚举值 */
    OH_NN_DEPTHWISE_CONV2D_NATIVE_PAD = 25,
    /** 当张量作为DepthwiseConv2dNative算子的dilation参数时，使用本枚举值 */
    OH_NN_DEPTHWISE_CONV2D_NATIVE_DILATION = 26,
    /** 当张量作为DepthwiseConv2dNative算子的padMode参数时，使用本枚举值 */
    OH_NN_DEPTHWISE_CONV2D_NATIVE_PAD_MODE = 27,
    /** 当张量作为DepthwiseConv2dNative算子的activationType参数时，使用本枚举值 */
    OH_NN_DEPTHWISE_CONV2D_NATIVE_ACTIVATION_TYPE = 28,

    /** 当张量作为Div算子的activationType参数时，使用本枚举值 */
    OH_NN_DIV_ACTIVATIONTYPE = 29,

    /** 当张量作为Eltwise算子的mode参数时，使用本枚举值 */
    OH_NN_ELTWISE_MODE = 30,

    /** 当张量作为FullConnection算子的axis参数时，使用本枚举值 */
    OH_NN_FULL_CONNECTION_AXIS = 31,
    /** 当张量作为FullConnection算子的activationType参数时，使用本枚举值 */
    OH_NN_FULL_CONNECTION_ACTIVATIONTYPE = 32,

    /** 当张量作为Matmul算子的transposeA参数时，使用本枚举值 */
    OH_NN_MATMUL_TRANSPOSE_A = 33,
    /** 当张量作为Matmul算子的transposeB参数时，使用本枚举值 */
    OH_NN_MATMUL_TRANSPOSE_B = 34,
    /** 当张量作为Matmul算子的activationType参数时，使用本枚举值 */
    OH_NN_MATMUL_ACTIVATION_TYPE = 35,

    /** 当张量作为MaxPool算子的kernel_size参数时，使用本枚举值 */
    OH_NN_MAX_POOL_KERNEL_SIZE = 36,
    /** 当张量作为MaxPool算子的stride参数时，使用本枚举值 */
    OH_NN_MAX_POOL_STRIDE = 37,
    /** 当张量作为MaxPool算子的pad_mode参数时，使用本枚举值 */
    OH_NN_MAX_POOL_PAD_MODE = 38,
    /** 当张量作为MaxPool算子的pad参数时，使用本枚举值 */
    OH_NN_MAX_POOL_PAD = 39,
    /** 当张量作为MaxPool算子的activation_type参数时，使用本枚举值 */
    OH_NN_MAX_POOL_ACTIVATION_TYPE = 40,

    /** 当张量作为Mul算子的activationType参数时，使用本枚举值 */
    OH_NN_MUL_ACTIVATION_TYPE = 41,

    /** 当张量作为OneHot算子的axis参数时，使用本枚举值 */
    OH_NN_ONE_HOT_AXIS = 42,

    /** 当张量作为Pad算子的constant_value参数时，使用本枚举值 */
    OH_NN_PAD_CONSTANT_VALUE = 43,

    /** 当张量作为Scale算子的activationType参数时，使用本枚举值*/
    OH_NN_SCALE_ACTIVATIONTYPE = 44,
    /** 当张量作为Scale算子的axis参数时，使用本枚举值*/
    OH_NN_SCALE_AXIS = 45,

    /** 当张量作为Softmax算子的axis参数时，使用本枚举值 */
    OH_NN_SOFTMAX_AXIS = 46,

    /** 当张量作为SpaceToBatchND算子的BlockShape参数时，使用本枚举值 */
    OH_NN_SPACE_TO_BATCH_ND_BLOCK_SHAPE = 47,
    /** 当张量作为SpaceToBatchND算子的Paddings参数时，使用本枚举值 */
    OH_NN_SPACE_TO_BATCH_ND_PADDINGS = 48,

    /** 当张量作为Split算子的Axis参数时，使用本枚举值 */
    OH_NN_SPLIT_AXIS = 49,
    /** 当张量作为Split算子的OutputNum参数时，使用本枚举值 */
    OH_NN_SPLIT_OUTPUT_NUM = 50,
    /** 当张量作为Split算子的SizeSplits参数时，使用本枚举值 */
    OH_NN_SPLIT_SIZE_SPLITS = 51,

    /** 当张量作为Squeeze算子的Axis参数时，使用本枚举值 */
    OH_NN_SQUEEZE_AXIS = 52,

    /** 当张量作为Stack算子的Axis参数时，使用本枚举值 */
    OH_NN_STACK_AXIS = 53,

    /** 当张量作为StridedSlice算子的BeginMask参数时，使用本枚举值 */
    OH_NN_STRIDED_SLICE_BEGIN_MASK = 54,
    /** 当张量作为StridedSlice算子的EndMask参数时，使用本枚举值 */
    OH_NN_STRIDED_SLICE_END_MASK = 55,
    /** 当张量作为StridedSlice算子的EllipsisMask参数时，使用本枚举值 */
    OH_NN_STRIDED_SLICE_ELLIPSIS_MASK = 56,
    /** 当张量作为StridedSlice算子的NewAxisMask参数时，使用本枚举值 */
    OH_NN_STRIDED_SLICE_NEW_AXIS_MASK = 57,
    /** 当张量作为StridedSlice算子的ShrinkAxisMask参数时，使用本枚举值 */
    OH_NN_STRIDED_SLICE_SHRINK_AXIS_MASK = 58,

    /** 当张量作为Sub算子的ActivationType参数时，使用本枚举值 */
    OH_NN_SUB_ACTIVATIONTYPE = 59,

    /** 当张量作为ReduceMean算子的keep_dims参数时，使用本枚举值*/
    OH_NN_REDUCE_MEAN_KEEP_DIMS = 60,

    /** 当张量作为ResizeBilinear算子的new_height参数时，使用本枚举值*/
    OH_NN_RESIZE_BILINEAR_NEW_HEIGHT = 61,
    /** 当张量作为ResizeBilinear算子的new_width参数时，使用本枚举值*/
    OH_NN_RESIZE_BILINEAR_NEW_WIDTH = 62,
    /** 当张量作为ResizeBilinear算子的preserve_aspect_ratio参数时，使用本枚举值*/
    OH_NN_RESIZE_BILINEAR_PRESERVE_ASPECT_RATIO = 63,
    /** 当张量作为ResizeBilinear算子的coordinate_transform_mode参数时，使用本枚举值*/
    OH_NN_RESIZE_BILINEAR_COORDINATE_TRANSFORM_MODE = 64,
    /** 当张量作为ResizeBilinear算子的exclude_outside参数时，使用本枚举值*/
    OH_NN_RESIZE_BILINEAR_EXCLUDE_OUTSIDE = 65,

    /** 当张量作为LayerNorm算子的beginNormAxis参数时，使用本枚举值 */
    OH_NN_LAYER_NORM_BEGIN_NORM_AXIS = 66,
    /** 当张量作为LayerNorm算子的epsilon参数时，使用本枚举值 */
    OH_NN_LAYER_NORM_EPSILON = 67,
    /** 当张量作为LayerNorm算子的beginParamsAxis参数时，使用本枚举值 */
    OH_NN_LAYER_NORM_BEGIN_PARAM_AXIS = 68,
    /** 当张量作为LayerNorm算子的elementwiseAffine参数时，使用本枚举值 */
    OH_NN_LAYER_NORM_ELEMENTWISE_AFFINE = 69,

    /** 当张量作为ReduceProd算子的keep_dims参数时，使用本枚举值*/
    OH_NN_REDUCE_PROD_KEEP_DIMS = 70,

    /** 当张量作为ReduceAll算子的keep_dims参数时，使用本枚举值*/
    OH_NN_REDUCE_ALL_KEEP_DIMS = 71,

    /** 当张量作为QuantDTypeCast算子的src_t参数时，使用本枚举值*/
    OH_NN_QUANT_DTYPE_CAST_SRC_T = 72,
    /** 当张量作为QuantDTypeCast算子的dst_t参数时，使用本枚举值*/
    OH_NN_QUANT_DTYPE_CAST_DST_T = 73,

    /** 当张量作为Topk算子的Sorted参数时，使用本枚举值 */
    OH_NN_TOP_K_SORTED = 74,

    /** 当张量作为ArgMax算子的axis参数时，使用本枚举值 */
    OH_NN_ARG_MAX_AXIS = 75,
    /** 当张量作为ArgMax算子的keepDims参数时，使用本枚举值 */
    OH_NN_ARG_MAX_KEEPDIMS = 76,

    /** 当张量作为Unsqueeze算子的Axis参数时，使用本枚举值 */
    OH_NN_UNSQUEEZE_AXIS = 77,
} OH_NN_TensorType;

/**
 * @brief 该结构体用于存储32位无符号整型数组。
 *
 * @since 9
 * @version 1.0
 */
typedef struct OH_NN_UInt32Array {
    /** 无符号整型数组的指针 */
    uint32_t *data;
    /** 数组长度 */
    uint32_t size;
} OH_NN_UInt32Array;

/**
 * @brief 量化信息。
 *
 * 在量化的场景中，32位浮点型数据根据以下公式量化为定点数据：
 \f[
    q = clamp(round(\frac{r}{s}+z), q_{min}, q_{max})
 \f]
 * 其中s和z是量化参数，在{@link OH_NN_QuantParam}中通过scale和zeroPoint保存，r是浮点数，q是量化后的结果，
 * q_min是量化结果的下界，q_max是量化结果的上界，计算方式如下：
 \f[
  \text{clamp}(x,min,max) = 
  \begin{cases}
       q_{min} = -(1 << (numBits - 1)) \\
       q_{max} = (1 << (numBits - 1)) \\
   \end{cases}
 \f]
 * clamp函数定义如下：
 \f[
  \text{clamp}(x,min,max) = 
  \begin{cases}
       \text{max} & \text{ if } x > \text{ max } \\
       \text{min} & \text{ if } x < \text{ min } \\
       x & \text{ otherwise } \\
   \end{cases}
 \f]
 * 
 * @deprecated since 11
 * @useinstead {@link NN_QuantParam}
 * @since 9
 * @version 1.0
 */
typedef struct OH_NN_QuantParam {
    /** 指定numBits、scale和zeroPoint数组的长度。在per-layer量化的场景下，quantCount通常指定为1，即一个张量所有通道
     *  共享一套量化参数；在per-channel量化场景下，quantCount通常和张量通道数一致，每个通道使用自己的量化参数。
     */
    uint32_t quantCount;
    /** 量化位数 */
    const uint32_t *numBits;
    /** 指向量化公式中scale数据的指针 */
    const double *scale;
    /** 指向量化公式中zero point数据的指针 */
    const int32_t *zeroPoint;
} OH_NN_QuantParam;

/**
 * @brief 张量结构体。
 *
 * 通常用于构造模型图中的数据节点和算子参数，在构造张量时需要明确数据类型、维数、维度信息和量化信息。
 *
 * @deprecated since 11
 * @useinstead {@link NN_TensorDesc}
 * @since 9
 * @version 1.0
 */
typedef struct OH_NN_Tensor {
    /** 指定张量的数据类型，要求从{@link OH_NN_DataType}枚举类型中取值。 */
    OH_NN_DataType dataType;
    /** 指定张量的维数 */
    uint32_t dimensionCount;
    /** 指定张量的维度信息（形状） */
    const int32_t *dimensions;
    /** 指定张量的量化信息，数据类型要求为{@link OH_NN_QuantParam}。 */
    const OH_NN_QuantParam *quantParam;
    /** 指定张量的类型。type的取值和张量的用途相关，当张量用作模型的输入或输出，则要求将type设置为{@link OH_NN_TENSOR}；
     *  当张量用作算子参数，则要求从{@link OH_NN_TensorType}中选择除{@link OH_NN_TENSOR}以外的枚举值。 
     */
    OH_NN_TensorType type;
} OH_NN_Tensor;

/**
 * @brief 内存结构体。
 *
 * @deprecated since 11
 * @useinstead {@link NN_Tensor}
 * @since 9
 * @version 1.0
 */
typedef struct OH_NN_Memory {
    /** 指向共享内存的指针，该共享内存通常由底层硬件驱动申请 */
    void * const data;
    /** 记录共享内存的字节长度 */
    const size_t length;
} OH_NN_Memory;

#ifdef __cplusplus
}
#endif // __cplusplus

/** @} */
#endif // NEURAL_NETWORK_RUNTIME_TYPE_H