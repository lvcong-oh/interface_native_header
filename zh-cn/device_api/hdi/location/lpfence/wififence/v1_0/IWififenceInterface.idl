/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceWififence
 * @{
 *
 * @brief 为低功耗围栏服务提供Wi-Fi围栏的API
 *
 * 本模块接口提供添加Wi-Fi围栏，删除Wi-Fi围栏，获取Wi-Fi围栏状态，获取Wi-Fi围栏使用信息的功能。
 * 应用场景：一般用于判断设备是否在室内特定位置，如居所内或商场的某个店铺内。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IWififenceInterface.idl
 *
 * @brief 定义Wi-Fi围栏模块回调接口。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @brief Wi-Fi围栏模块接口的包路径。
 *
 * @since 4.0
 */
package ohos.hdi.location.lpfence.wififence.v1_0;

/**
 * @brief 导入Wi-Fi围栏模块的数据类型。
 *
 * @since 4.0
 */
import ohos.hdi.location.lpfence.wififence.v1_0.WififenceTypes;

/**
 * @brief 导入Wi-Fi围栏模块的回调函数定义。
 *
 * @since 4.0
 */
import ohos.hdi.location.lpfence.wififence.v1_0.IWififenceCallback;

/**
 * @brief 定义对Wi-Fi围栏模块进行基本操作的接口。
 *
 * 接口包含注册回调函数，取消注册回调函数，添加Wi-Fi围栏，删除Wi-Fi围栏，获取Wi-Fi围栏状态，获取Wi-Fi围栏使用信息的功能。
 */
interface IWififenceInterface {
    /**
     * @brief 注册回调函数。
     *
     * 用户在开启Wi-Fi围栏功能前，需要先注册该回调函数。当Wi-Fi围栏状态发生变化时，会通过回调函数进行上报。
     *
     * @param callbackObj 要注册的回调函数，只需成功订阅一次，无需重复订阅。详见{@link IWififenceCallback}。
     *
     * @return 如果注册回调函数成功，则返回0。
     * @return 如果注册回调函数失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    RegisterWififenceCallback([in] IWififenceCallback callbackObj);

    /**
     * @brief 取消注册回调函数。
     *
     * 取消之前注册的回调函数。当不需要使用Wi-Fi围栏功能，或需要更换回调函数时，需要取消注册回调函数。
     *
     * @param callbackObj 要取消注册的回调函数，只需成功取消订阅一次，无需重复取消订阅。详见{@link IWififenceCallback}。
     *
     * @return 如果取消注册回调函数成功，则返回0。
     * @return 如果取消注册回调函数失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    UnregisterWififenceCallback([in] IWififenceCallback callbackObj);

    /**
     * @brief 添加Wi-Fi围栏。
     *
     * 支持一次添加多个Wi-Fi围栏，一个Wi-Fi围栏包含多组MAC地址信息。
     *
     * @param wififence 添加的Wi-Fi围栏信息。详见{@link WififenceRequest}。
     *
     * @return 如果添加成功，则返回0。
     * @return 如果添加失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    AddWififences([in] struct WififenceRequest[] wififence);

    /**
     * @brief 删除Wi-Fi围栏。
     *
     * 支持一次删除多个Wi-Fi围栏。
     *
     * @param wififenceId Wi-Fi围栏ID号。详见{@link WififenceRequest}。
     *
     * @return 如果删除成功，则返回0。
     * @return 如果删除失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    RemoveWififences([in] int[] wififenceId);

    /**
     * @brief 获取设备与一个Wi-Fi围栏的状态关系。
     *
     * 设备与Wi-Fi围栏的状态关系详见{@link WififenceTransition}定义。
     *
     * @param wififenceId Wi-Fi围栏ID号。
     *
     * @return 返回位置关系。详见{@link WififenceTransition}定义。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetWififenceStatus([in] int wififenceId);

    /**
     * @brief 获取Wi-Fi围栏使用信息
     *
     * 查看当前设备支持添加的Wi-Fi围栏最大个数和已添加的Wi-Fi围栏个数。通过回调函数上报通知，详见{@link OnGetWififenceSizeCb}。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetWififenceSize();
}
/** @} */