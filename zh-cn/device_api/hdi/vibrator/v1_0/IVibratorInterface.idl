/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief 马达驱动对马达服务提供通用的接口能力。
 *
 * 模块提供马达服务对马达驱动访问的统一接口，服务获取驱动对象或者代理后，控制马达的单次振动、周期性振动、停止振动。
 *
 * @since 2.2
 * @version 1.0
 */

/**
 * @file IVibratorInterface.idl
 *
 * @brief 定义马达的通用API，可用于控制马达执行单次或周期性振动。
 *
 * @since 2.2
 * @version 1.0
 */

/**
 * @brief Vibrator模块的包路径。
 * 
 * @since 2.2
 * @version 1.0
 */
package ohos.hdi.vibrator.v1_0;

import ohos.hdi.vibrator.v1_0.VibratorTypes;

 /**
 * @brief Vibrator模块向上层服务提供统一的接口。
 *
 * 上层服务开发人员可根据Vibrator模块提供的统一接口，用于控制马达执行单次或周期性振动。
 *
 * @since 2.2
 * @version 1.0
 */

interface IVibratorInterface {
    /**
     * @brief 控制马达以执行给定持续时间的单次振动。
     *
     * 单次振动与周期振动相互排斥。在执行单次振动之前，需退出周期性振动。
     *
     * @param duration 表示单次振动的持续时间，以毫秒为单位。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    StartOnce([in] unsigned int duration);
    /**
     * @brief 控制马达以预置效果执行周期性振动。
     *
     * 单次振动与周期振动相互排斥。在执行周期性振动之前，需退出单次振动。
     *
     * @param effectType 表示单次振动的预设效果。建议最大长度为64字节。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    Start([in] String effectType);
    /**
     * @brief 停止马达振动。
     *
     * 马达启动前，必须在任何模式下停止振动。此功能用在振动过程之后。
     *
     * @param mode 表示振动模式，可以是单次或周期性的，详见{@link HdfVibratorMode}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    Stop([in] enum HdfVibratorMode mode);
}
/** @} */
