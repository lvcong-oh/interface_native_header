/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_MANAGER_H
#define OHOS_AVSESSION_MANAGER_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief Provides APIs for audio and video (AV) media control.
 *
 * The APIs can be used to create, manage, and control media sessions.
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_manager.h
 *
 * @brief Declares APIs of the AV session manager.
 *
 * @since 9
 * @version 1.0
 */

#include <functional>
#include <string>
#include <memory>

#include "audio_system_manager.h"
#include "av_session.h"
#include "avsession_controller.h"
#include "avsession_info.h"
#include "key_event.h"

namespace OHOS::AVSession {
/**
 * @brief Implements APIs of the AV session manager.
 *
 * @since 9
 * @version 1.0
 */
class AVSessionManager {
public:
    /**
     * @brief Obtains a session manager instance.
     *
     * @return Returns the session manager instance.
     * @since 9
     * @version 1.0
     */
    static AVSessionManager& GetInstance();

    /**
     * @brief Creates an AV session.
     *
     * @param tag Indicates the pointer to the tag for the session, which cannot be null.
     * @param type Indicates the session type,
     * which can be {@link AVSession#SESSION_TYPE_AUDIO} or {@link AVSession#SESSION_TYPE_VIDEO}.
     * @param elementName Indicates the pointer to the session name, which is an {@link AppExecFwk::ElementName} object.
     * @return Returns the pointer to the session created.
     * @since 9
     * @version 1.0
     */
    virtual std::shared_ptr<AVSession> CreateSession(const std::string& tag, int32_t type,
                                                     const AppExecFwk::ElementName& elementName) = 0;

    /**
     * @brief Obtains all the AV session descriptors.
     *
     * @param descriptors Indicates the pointer to an array of session descriptors,
     * each of which is an {@link AVSessionDescriptor} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetAllSessionDescriptors(std::vector<AVSessionDescriptor>& descriptors) = 0;

    /**
     * @brief Obtains all the activated AV session descriptors.
     *
     * @param activatedSessions Indicates the pointer to an array of activated session descriptors,
     * each of which is an {@link AVSessionDescriptor} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetActivatedSessionDescriptors(std::vector<AVSessionDescriptor>& activatedSessions) = 0;

    /**
     * @brief Obtains a session descriptor by session ID.
     *
     * @param sessionId Indicates the pointer to the session ID.
     * @param descriptor Indicates the pointer to a session descriptor, which is an {@link AVSessionDescriptor} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetSessionDescriptorsBySessionId(const std::string& sessionId, AVSessionDescriptor& descriptor) = 0;

    /**
     * @brief Creates an AV session controller.
     *
     * @param sessionId Indicates the pointer to the session ID.
	 * @param controller Indicates the pointer to a session controller instance,
     * which is an {@link AVSessionController} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t CreateController(const std::string& sessionId,
        std::shared_ptr<AVSessionController>& controller) = 0;

    /**
     * @brief Registers an AV session listener.
     *
     * @param listener Indicates the pointer to the session listener, which is a {@link SessionListener} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t RegisterSessionListener(const std::shared_ptr<SessionListener>& listener) = 0;

    /**
     * @brief Registers a death callback for the AV session server.
     *
     * @param callback Indicates the pointer to the callback, which is a {@link DeathCallback} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @see UnregisterServiceDeathCallback
     * @since 9
     * @version 1.0
     */
    virtual int32_t RegisterServiceDeathCallback(const DeathCallback& callback) = 0;

    /**
     * @brief Deregisters the death callback for the AV session server.
     *
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @see RegisterServiceDeathCallback
     * @since 9
     * @version 1.0
     */
    
    virtual int32_t UnregisterServiceDeathCallback() = 0;

    /**
     * @brief Sends a system key event.
     *
     * @param keyEvent Indicates the pointer to the key event code, which is an {@link MMI::KeyEvent} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t SendSystemAVKeyEvent(const MMI::KeyEvent& keyEvent) = 0;

    /**
     * @brief Sends a control command.
     *
     * @param command Indicates the pointer to the control command, which is an {@link AVControlCommand} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t SendSystemControlCommand(const AVControlCommand& command) = 0;
    
    /**
     * @brief Casts an AV session to a remote or local device.
     *
     * @param token Indicates the pointer to the session token.
     * @param descriptors Indicates the pointer to the target device.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t CastAudio(const SessionToken& token,
                              const std::vector<AudioStandard::AudioDeviceDescriptor>& descriptors) = 0;
    
     /**
     * @brief Casts all AV sessions from this device to a remote device.
     *
     * @param descriptors Indicates the pointer to the target device.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t CastAudioForAll(const std::vector<AudioStandard::AudioDeviceDescriptor>& descriptors) = 0;
};
} // namespace OHOS::AVSession
/** @} */
#endif // OHOS_AVSESSION_MANAGER_H
